output: romandigitconverter.o numberconversion.o
	g++ $^ -o $@

romandigitconverter.o: romandigitconverter.cpp
	g++ -c romandigitconverter.cpp

numberconversion.o: numberconversion.cpp numberconversion.h
	g++ -c numberconversion.cpp

.PHONY : clean
clean:
	rm *.o
	rm output

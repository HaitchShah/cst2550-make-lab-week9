#include "numberconversion.h"

int value(char r){
  if(r == 'I')
    return 1;
  if(r == 'V')
    return 5;
  if(r == 'X')
    return 10;
  if(r == 'L')
    return 50;
  if(r == 'C')
    return 100;
  if(r == 'D')
    return 500;
  if(r == 'M')
    return 1000;

  return -1;
}

int romantoint(std::string roman_numeral)
{
  int result = 0;

  for(int i = 0; i < roman_numeral.length(); i++){

    int s1 = value(roman_numeral[i]);

    if(i + 1 < roman_numeral.length()){
      int s2 = value(roman_numeral[i+1]);

      if(s1 >= s2){
	result = result + s1;
      }else{
	result = result + s2 - s1;
	i++;
      }
      
    }else{
      result = result + s1;
    }
    
    
  }
  
  return result;
}

std::string inttoroman(int number)
{
  std::string str_romans[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
  int values[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

  std::string result = "";

  for(int i = 0; i < 13; ++i){
    while(number - values[i] >= 0){
      result += str_romans[i];
      number -= values[i];
    }
  }
  
  return result;
}
